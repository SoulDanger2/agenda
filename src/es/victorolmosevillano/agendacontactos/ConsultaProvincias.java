/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.agendacontactos;

import javax.persistence.Query;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Victor
 */
public class ConsultaProvincias {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("AgendaContactosPU");
        EntityManager em = emf.createEntityManager();

        //String jpql = "SELECT a FROM Provincias a"; 
        //Query query = em.createQuery(jpql);
        //Muestra una lista de provincias.
        System.out.println("Consulta 1");
        System.out.println("----------");
        Query queryProvincias = em.createNamedQuery("Provincia.findAll");
        List<Provincia> listProvincias = queryProvincias.getResultList();
        for (Provincia provincia : listProvincias) {
            System.out.println(provincia.getNombre());
        }
        System.out.println("-----------------------------------------------");
        System.out.println("Consulta 2");
        System.out.println("----------");

        /*crea un bucle con una variable que vale 0, que ira aumentando hasta un numero menos
        que el tamaño maximo de la provincia*/
        for (int i = 0; i < listProvincias.size(); i++) {
            Provincia provincia = listProvincias.get(i);
            System.out.println(provincia.getNombre());
        }

        System.out.println("-----------------------------------------------");
        System.out.println("Consulta 3");
        System.out.println("----------");
        //Muestra la id de provincia que desee, si no sale mensaje advirtiendo.
        Provincia provinciaId19 = em.find(Provincia.class, 229);
        if (provinciaId19 != null) {
            System.out.print(provinciaId19.getId() + ": ");
            System.out.println(provinciaId19.getNombre());
        } else {
            System.out.println("No hay ninguna provincia con ID=229");
        }

        //Añade datos a la base de datos.
        Query queryProvinciaCadiz = em.createNamedQuery("Provincia.findByNombre");
        queryProvinciaCadiz.setParameter("nombre", "Cádiz");
        List<Provincia> listProvinciasCadiz = queryProvinciaCadiz.getResultList();
        em.getTransaction().begin();
        for (Provincia provinciaCadiz : listProvinciasCadiz) {
            provinciaCadiz.setCodigo("11");
            em.merge(provinciaCadiz);
        }
        em.getTransaction().commit();

        //remueve datos de la base de datos.
        Provincia provinciaId2 = em.find(Provincia.class, 2);
        if (provinciaId2 != null) {
            em.remove(provinciaId2);
        } else {
            System.out.println("No hay ninguna provincia con ID= 2");
        }

             //Query queryTotalHijos = em.createNamedQuery("Persona.findByNumHijos");
    }

}
